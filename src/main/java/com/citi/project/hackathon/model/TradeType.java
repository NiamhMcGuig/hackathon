package com.citi.project.hackathon.model;

public enum TradeType{

    BUY, SELL
}
