package com.citi.project.hackathon.model;

import java.time.LocalDate;

public class Trade {

	// variables
	private String id;
    private LocalDate dateCreated;
    private String stockTicker;
    private int stockQuantity;
    private double requestedPrice;
    private TradeType tradeType;
    private TradeState tradeState;

    // constructors
    public Trade() {}

    public Trade(LocalDate dateCreated, String stockTicker, int stockQuantity, double requestedPrice, TradeType tradeType,
            TradeState tradeState) {
        this.dateCreated = dateCreated;
        this.stockTicker = stockTicker;
        this.stockQuantity = stockQuantity;
        this.requestedPrice = requestedPrice;
        this.tradeType = tradeType;
        this.tradeState = tradeState;
    }

	// gets and sets
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public LocalDate getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(LocalDate dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getStockTicker() {
		return this.stockTicker;
	}

	public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}

	public int getStockQuantity() {
		return this.stockQuantity;
	}

	public void setStockQuantity(int stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public double getRequestedPrice() {
		return this.requestedPrice;
	}

	public void setRequestedPrice(double requestedPrice) {
		this.requestedPrice = requestedPrice;
	}

	public TradeType getTradeType() {
		return this.tradeType;
	}

	public void setTradeType(TradeType tradeType) {
		this.tradeType = tradeType;
	}

	public TradeState getTradeState() {
		return this.tradeState;
	}

	public void setTradeState(TradeState tradeState) {
		this.tradeState = tradeState;
	}

    @Override
    public String toString() {
        return "Trade [dateCreated=" + dateCreated + ", requestedPrice=" + requestedPrice + ", stockQuantity="
                + stockQuantity + ", stockTicker=" + stockTicker + ", tradeState=" + tradeState + ", tradeType="
                + tradeType + "]";
    }

	

}
