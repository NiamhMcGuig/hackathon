package com.citi.project.hackathon.service;

import java.time.LocalDate;

import com.citi.project.hackathon.model.Trade;
import com.citi.project.hackathon.model.TradeState;
import com.citi.project.hackathon.model.TradeType;

import org.junit.jupiter.api.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;



@SpringBootTest
public class TradeServiceTest {

    private static final Logger LOG = LoggerFactory.getLogger(TradeServiceTest.class);

    @Autowired
    private TradeService tradeService;

    @Test
    public void test_save_sanityCheck() {

        LOG.debug("This is a message");

        Trade testTrade = new Trade();

        
        testTrade.setDateCreated(LocalDate.now());
        testTrade.setRequestedPrice(10);
        testTrade.setStockQuantity(1000);
        testTrade.setStockTicker("FB");
        testTrade.setTradeState(TradeState.CREATED);
        testTrade.setTradeType(TradeType.BUY);
        

        tradeService.save(testTrade);
        
    }

    @Test
    public void test_findAll_sanityCheck() {
        assert(tradeService.findAll().size() == 0);
    }
}