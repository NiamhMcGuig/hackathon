package com.citi.project.hackathon.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.List;

import com.citi.project.hackathon.model.Trade;
import com.citi.project.hackathon.model.TradeState;
import com.citi.project.hackathon.model.TradeType;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TradeControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void test_save_findAll() {
        Trade testTrade = new Trade();

        testTrade.setDateCreated(LocalDate.now());
        testTrade.setRequestedPrice(10);
        testTrade.setStockQuantity(1000);
        testTrade.setStockTicker("FB");
        testTrade.setTradeState(TradeState.CREATED);
        testTrade.setTradeType(TradeType.BUY);

        ResponseEntity<Trade> response = restTemplate.postForEntity("/v1/trade",
                                                                       testTrade,
                                                                       Trade.class);

        assertEquals(response.getStatusCode(), HttpStatus.CREATED);

        ResponseEntity<List<Trade>> findAllResponse = restTemplate.exchange(
                                                        "/v1/trade",
                                                        HttpMethod.GET,
                                                        null,
                                                        new ParameterizedTypeReference<List<Trade>>(){});

        assertEquals(findAllResponse.getStatusCode(), HttpStatus.OK);
    }

    
}